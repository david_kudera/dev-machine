#!/usr/bin/env bash

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

docker build -t dev-machine-base $SCRIPT_DIR/../images/base
docker build -t dev-machine-apps --build-arg baseImage=dev-machine-base $SCRIPT_DIR/../images/apps
