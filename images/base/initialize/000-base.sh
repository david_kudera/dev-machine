#!/usr/bin/env bash

set -e

sudo chown code:code /home/code

mkdir -p /home/code/Projects
mkdir -p /home/code/.ssh
chmod 700 /home/code/.ssh
touch /home/code/.ssh/authorized_keys
chmod 600 /home/code/.ssh/authorized_keys
cp -a /__home/. /home/code/

sudo chown -R code:code /home/code

# Connect to additional docker networks
if [ -n "$JOIN_ADDITIONAL_NETWORK" ]; then
	# It may be already connected to the additional network
    sudo docker network connect "$JOIN_ADDITIONAL_NETWORK" "$(hostname)" | true
fi

# Check if SSH_PUBLIC_KEY environment variable is set and not empty
if [ -n "$SSH_PUBLIC_KEY" ]; then
    echo "$SSH_PUBLIC_KEY" > ~/.ssh/authorized_keys
fi

# Configure GIT
if [ -n "$GIT_USER_NAME" ]; then
	git config --global user.name "$GIT_USER_NAME"
fi
if [ -n "$GIT_USER_EMAIL" ]; then
	git config --global user.email "$GIT_USER_EMAIL"
fi
if [ -n "$GIT_SIGNING_KEY" ]; then
	git config --global user.signingkey "$GIT_SIGNING_KEY"
	git config --global gpg.format "ssh"
	git config --global commit.gpgsign "true"
fi

# Configure shell
if [ -n "$USER_SHELL" ]; then
	sudo chsh -s "$USER_SHELL" code
fi

# Install dotfiles
if [ -n "$DOTFILES" ]; then
	DOTFILES_DIR="/home/code/Projects/.dotfiles"

	if [ -d "$DOTFILES_DIR" ]; then
		git -C "$DOTFILES_DIR" pull --recurse-submodules;
	else
		GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=accept-new" git clone --recursive "$DOTFILES" "$DOTFILES_DIR"
	fi

	if [ -f "$DOTFILES_DIR/install.sh" ]; then
		"$DOTFILES_DIR/install.sh"
	fi
fi
