# dev-machine/base

Extends [ubuntu:22.04](https://hub.docker.com/_/ubuntu)

**Image:**

`registry.gitlab.com/david_kudera/dev-machine/base:latest`

**Run:**

```bash
$ docker run \
    --name="DevMachine.Base" \
    --privileged=true \
    -e SSH_PUBLIC_KEY="..." \
    -e TZ="..." \
    -e GIT_SIGNING_KEY="..." \
    -e GIT_USER_NAME="..." \
    -e GIT_USER_EMAIL="..." \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /path/to/dev_machine/:/home/code \
    registry.gitlab.com/david_kudera/dev-machine/base:latest
```

## Included

* docker
* ssh server
* ssh agent forwarding
* ssh X11 forwarding
* node.js
* bun
* dotnet

## Environment variables

* `SSH_PUBLIC_KEY`
* `GIT_USER_NAME`
* `GIT_USER_EMAIL`
* `GIT_SIGNING_KEY`
* `JOIN_ADDITIONAL_NETWORK`
* `USER_SHELL`
* `DOTFILES`

## Ports

* `22`: ssh

## Volumes

* `/home/code`: `code` user home directory

## SSH Connection

**`~/.ssh/config`:**

```
Host dev
    HostName <IP_Address>
    User code
    ForwardAgent yes        # Optional
    ServerAliveInterval 30  # Optional
```

```bash
$ ssh dev
```
