#!/usr/bin/env bash

set -e

for script in /__run/*.sh; do
    [ -f "$script" ] && [ -x "$script" ] && "$script"
done

tail -f /dev/null
