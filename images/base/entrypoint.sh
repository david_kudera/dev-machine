#!/usr/bin/env bash

set -e

INITIALIZED_FILE="/.__initialized"

# Configure container
if [ ! -f "$INITIALIZED_FILE" ]; then
	echo "Running initial configuration..."

	for script in /__initialize/*.sh; do
	    [ -f "$script" ] && [ -x "$script" ] && "$script"
	done

	sudo touch "$INITIALIZED_FILE"
fi

# Execute any command passed to the script
exec "$@"
