#!/usr/bin/env bash

set -e

# Start SSH server as a background process
if [ -n "$SSH_PUBLIC_KEY" ]; then
	echo "Enabling SSH server..."
	sudo /usr/sbin/sshd
fi
