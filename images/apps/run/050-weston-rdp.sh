#!/usr/bin/env bash

set -e

sudo winpr-makecert -rdp -n "DevMachine" -path /etc/xdg/weston
sudo mv /etc/xdg/weston/DevMachine.crt /etc/xdg/weston/rdp.crt
sudo mv /etc/xdg/weston/DevMachine.key /etc/xdg/weston/rdp.key

if [ "$START_WESTON_RDP" = "true" ]; then
	weston --backend=rdp --rdp-tls-cert=/etc/xdg/weston/rdp.crt --rdp-tls-key=/etc/xdg/weston/rdp.key &
fi
