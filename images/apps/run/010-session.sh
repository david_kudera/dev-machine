#!/usr/bin/env bash

set -e

dbus-daemon --address="$DBUS_SESSION_BUS_ADDRESS" --config-file=/usr/share/dbus-1/session.conf --fork
