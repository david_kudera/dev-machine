# dev-machine/apps

## Included

* Waypipe
* Firefox
* Weston
* Nautilus
* Gnome Terminal
* Xpra
* VirtualGL
* GitHub Desktop
* JetBrains Toolbox

## Environment variables

* `START_WESTON_RDP`

## Client requirements

* [PyOpenGL-accelerate](https://pypi.org/project/PyOpenGL-accelerate/)
